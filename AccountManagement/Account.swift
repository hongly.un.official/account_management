//
//  Account.swift
//  AccountManagement
//
//  Created by hongly on 11/8/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation

class Account {
    
    var userName: String
    var password: String
    
    init(userName: String, password: String) {
        self.userName = userName
        self.password = password
    }
}
