//
//  LoginViewController.swift
//  AccountManagement
//
//  Created by hongly on 11/8/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    var accountModelController: AccountModelController?
    var homeVCDelegate: HomeViewControllerDelegate!
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var alertLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userNameTextField.delegate = self
        self.passwordTextField.delegate = self
        
        userNameTextField.placeholder = "User Name"
        passwordTextField.placeholder = "Password"
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
            if let signUpViewController = segue.destination as? SingUpViewController {
                signUpViewController.accountModelController = accountModelController
            }
        
            if let signUpViewController = segue.destination as? SingUpViewController {
                signUpViewController.accountModelController = accountModelController
            }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
            return false
    }
    
    @IBAction func loginButtonTap(_ sender: UIButton) {
        
            guard let userName = userNameTextField.text, userName != "" else {
                alertLabel.text = "Please input user name!"
                userNameTextField.becomeFirstResponder()
                return
            }
        
            guard let password = passwordTextField.text, password != "" else {
                alertLabel.text = "Please input password!"
                passwordTextField.becomeFirstResponder()
                return
            }
        
            if accountModelController!.accounts.count  >  0  {
            
                    var account: Account? = nil
                
                    for temp_account in accountModelController!.accounts {
                        if temp_account.userName == userName {
                            account = temp_account
                        }
                    }
              
                    if account == nil {
                        alertLabel.text = "Not registered"
                    } else if account?.password != password  {
                        alertLabel.text = "Incorrect Password"
                    } else {
                        homeVCDelegate.wellcome(name: account!.userName)
                        dismiss(animated: true, completion: nil)
                    }
            }  else {
                 alertLabel.text = "Not registered"
            }
    }

    @IBAction func signupButtonTap(_ sender: UIButton) {
        
    }
}
