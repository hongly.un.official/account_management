//
//  SingUpViewController.swift
//  AccountManagement
//
//  Created by hongly on 11/8/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit

class SingUpViewController: UIViewController,UITextFieldDelegate {

    var accountModelController: AccountModelController?
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var alertLabel: UILabel!
    

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.userNameTextField.delegate = self
        self.passwordTextField.delegate = self
        self.confirmPasswordTextField.delegate = self

        // Do any additional setup after loading the view.
        userNameTextField.placeholder = "User Name"
        passwordTextField.placeholder = "Password"
        confirmPasswordTextField.placeholder = "Confirm Password"
    }
    

    @IBAction func createButton(_ sender: Any) {
    
        
        guard  let userName = userNameTextField.text, userName != "" else {
            alertLabel.text = "Please input user name!"
            userNameTextField.becomeFirstResponder()
            return
        }
        guard let password = passwordTextField.text, password != "" else {
            alertLabel.text = "Please input password!"
            passwordTextField.becomeFirstResponder()
            return
        }
    
        guard let confirmPassword = confirmPasswordTextField.text, confirmPassword != "" else {
            alertLabel.text = "Please confirm your password!"
            confirmPasswordTextField.becomeFirstResponder()
            return
        }
        
        
        if password == confirmPassword {
           //Register Account
            accountModelController?.accounts.append(Account(userName: userName, password: password))
            
            dismiss(animated: true, completion: nil)
            
        }else {
            alertLabel.text = "Password did not match!"
        }
    
    }
    
    
    @IBAction func backButtonTap(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
}
