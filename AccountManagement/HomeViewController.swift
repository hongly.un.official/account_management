//
//  ViewController.swift
//  AccountManagement
//
//  Created by hongly on 11/8/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit


protocol HomeViewControllerDelegate {
    func wellcome(name: String)
}

class HomeViewController: UIViewController, HomeViewControllerDelegate{
  
    
    var accountModelController: AccountModelController?
    
    @IBOutlet weak var wellcomeLabel: UILabel!
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let loginViewController = segue.destination as? LoginViewController {
            loginViewController.accountModelController = accountModelController
            loginViewController.homeVCDelegate = self
            
        }
    }

    @IBAction func loginButtonTab(_ sender: UIButton) {
        
        if  loginButton.currentTitle ==  "Log Out" {
            wellcomeLabel.text = ""
            loginButton.setTitle("Login", for: .normal)
            loginButton.backgroundColor = #colorLiteral(red: 0.4464123954, green: 0.2934940449, blue: 0, alpha: 1)
            return
        }else{
                self.performSegue(withIdentifier: "a", sender: self)
        }
    }
    
    func wellcome(name: String) {
        
         loginButton.setTitle("Log Out", for: .normal)
        loginButton.backgroundColor = UIColor.red
        wellcomeLabel.text = "Wellcome \(name) !"
    }	

}

